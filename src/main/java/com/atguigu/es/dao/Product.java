package com.atguigu.es.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document(indexName = "product", shards = 3, replicas = 1)
public class Product {
    @Id // 必须有 全局唯一标识 等同于 es 的 _id
private Long id;//商品唯一标识
    /**
     * type : 字段数据类型
     * analyzer : 分词器类型
     * index : 是否索引(默认:true)
     * Keyword : 短语,不进行分词
     */
@Field(type = FieldType.Text ,analyzer ="ik_max_word")
private String title;//商品名称尚硅谷技术之 Elasticsearch
@Field(type=FieldType.Keyword) // 关键字不能分词
private String category;//分类名称
@Field(type = FieldType.Keyword,index =false) // index =false  不能被查询
private Double price;//商品价格
private String images;//图片地址
}