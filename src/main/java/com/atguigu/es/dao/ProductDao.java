package com.atguigu.es.dao;

import com.atguigu.es.config.ElasticsearchConfig;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDao   extends ElasticsearchRepository<Product,Long> {
}
